#!/usr/bin/env python

'''
SNPpy V 1.0
Cullen McGovern
10/15/14
mcgoverncl@gmail.com
'''

import csv
import os.path

#FUNCTION DEFINITIONS

#Takes a reference file path, a start posistion, and a stop position. Returns a list of bases with chromosome addresses. Currently, ref must have fasta > header removed.
def get_ref_seq(ref, low, high):
    index = 0
    ref_lst = []
    print '\nopening reference file at %s' % (ref)
    with open(ref, 'r') as f:
        while True:
            base = f.read(1)
            if base != '\n':
	        index += 1
                if index in range(low, high + 1):
                    ref_lst.append([index, base])
                elif index > high:
                    break
    return ref_lst

# Read snps from file and return 3xn list
def get_snps(snp, loc, low, high, val):
    snp_lst = []
    with open(snp, 'r') as f:
        reader = csv.reader(f, delimiter = '\t')
        for sample, chromosome, position, original, new, quality, support, concordance, hits in reader:
            if chromosome.lower() in loc and int(position) in range(low, high + 1) and int(quality) >= val:
		snp_lst.append([int(position), original, new])
    print '%s SNPs found' % (len(snp_lst))
    return snp_lst


# Swap bases. Some confusing (confused?) index math.
def swap(ref_lst, snp_lst):
    new = []
    count = 0
    for i in ref_lst:
        new.append(i[1])
    for n, j in enumerate(new):
        for k in snp_lst:
            if int(k[0]) - int(ref_lst[0][0]) == n and k[1] == new[n]:
                new[n] = k[2]
                count += 1
    print '%s SNPs processed' % (str(count))
    return ''.join(new)
    
#BODY

print '\n SNPpy V1.0\n============\n \/      \/\n || 0  0 ||\n \\\\_|__|_//\n (___<>___)\n ////  \\\\\\\\ \n ||||  ||||\n'

#Assign current working directory
cwd = os.getcwd()

#Dictionary relating 'chrn' (as found in 1001 files) string with path to corresponding TAIR10 Col-0 full chromosome sequence
ref_files = {
    '1' : cwd + '/ref_files/TAIR10_chr1.fas',
    '2' : cwd + '/ref_files/TAIR10_chr2.fas',
    '3' : cwd + '/ref_files/TAIR10_chr3.fas',
    '4' : cwd + '/ref_files/TAIR10_chr4.fas',
    '5' : cwd + '/ref_files/TAIR10_chr5.fas',
    'chloroplast6' : cwd + '/ref_files/TAIR10_chrC.fas',
    'mitochondria7' : cwd + '/ref_files/TAIR10_chrM.fas'
}

#Assign chromosome of interest
chrom = raw_input('Please enter a chromosome number between 1 and 5 or enter "c" for chloroplast or "m" for mitochochindrial sequences.\n> ')
if chrom == 'c':
    chrom = 'chloroplast6'
if chrom == 'm':
    chrom = 'mitochondria7'
    
#Assign working refernce path
ref_file = ref_files[chrom]

#Read in start and stop position as integers
start = int(raw_input('Enter start position.\n> '))
stop = int(raw_input('Enter stop position. \n> '))

#get minimum quality score
qual = int(raw_input('Enter a minimum quality score (inclusive) for SNPs to be processed. Scores less than 20 are not considered reliable. Scores above 25 are considered very reliable.\n> '))

#get file tag
tag = raw_input('Enter a tag to be appended to your translated files before the .fas extenstion.\n> ').lower()

#create short sequence with correct chromosomal index
ref_seq = get_ref_seq(ref_file, start, stop)

#iterate over files in snp_files to generate new fasta files.
for i in os.listdir(cwd + '/snp_files'):
    path = cwd + '/snp_files/' + i
    if not path.endswith('~') and path.endswith('.txt'):
        print '\nconverting %s to fasta format' % (i)
        snps = get_snps(path, chrom, start, stop, qual)
        with open(cwd + '/output/' + i[:-4] + '_' + tag + '.fas', 'w') as f:
            f.write('>' + i[:-4] + '_' + tag + '\n' + swap(ref_seq, snps))
    print 'done'

print '\n'


