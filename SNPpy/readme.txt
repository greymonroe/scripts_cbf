 SNPpy V1.0
============
 \/      \/
 || 0  0 ||
 \\_|__|_//
 (___<>___)
 ////  \\\\
 ||||  ||||

SNPpy the Crab uses his pincers to turn 1001 genomes files into fasta files!

As evidenced by this readme, SNPpy is an very unprofessional program. If you give him incorrect input, he'll take it, and if you're lucky, he'll have a meltdown. If you're not lucky, he'll quietly give you bad output.

SO... read this.

SNPpy comes pre-loaded with the TAIR10 Col-0 genome in the 'ref_files' folder. Don't mess with it.

He can be very slow to get going depending on where your gene is on the chromosome, but he's not frozen.

Put any 1001 genomes files you want converted into the 'snp_files' folder. You have to remove them when you're done if you don't want SNPpy to convert them again on his next run. No subfolders. Files must end in .txt

SNPpy's output goes in the 'output' folder. Feel free to let this fill up, SNPpy will name the new fasta files after the old 1001 genomes files, so there shouldn't be duplicates.

SNP.py is the python code that makes SNPpy tick. Mess with it if you want to.

DO NOT rename or remove any of the folders or files in the SNPpy directory. You should have: output, ref_files, snp_files, readme.txt and SNP.py.

Feel free to add any folders or files you want to the main SNPpy directory, he won't mind.

To run SNPpy:

1. Don't use windows, ever.

2. You'll need to have python installed. If you're running some kind of GNU/linux or OS X, you probably have it. If you don't have it, please refer to the internet.

3. Open a terminal with ctrl+alt+T (linux) or by searching for 'terminal' in spotlight for OS X.

4. In the terminal, navigate to the SNPpy directory by first typing 'ls' to see everything in your home directory, then type 'cd' followed by the name of a directory (case matters) to move into that directory. Repeat this process until you arrive at SNPpy. You must be in the SNPpy directory to use the program.

If you know how to script, I'm sure you can find a better solution.

5. Type './SNP.py'

6. Follow the prompts.

7. If your gene reads on the reverse strand, the output will need to be reverse complemented. SNPpy does not do this. Consider this option if it seems like the output is wrong.


