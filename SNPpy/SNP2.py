#!/usr/bin/env python

'''
SNPpy V 1.0
Cullen McGovern
10/15/14
mcgoverncl@gmail.com
'''

import csv
import os.path

# Read snps from file and return 3xn list
def get_snps(snp, loc, val):
    snp_lst = []
    with open(snp, 'r') as f:
        reader = csv.reader(f, delimiter = '\t')
        for sample, chromosome, position, original, new, quality, support, concordance, hits in reader:
            if chromosome == 'Chr4' and int(quality) >= val:
		snp_lst.append([int(position), original, new])
    print '%s SNPs found' % (len(snp_lst))
    return snp_lst



# Swap bases. Some confusing (confused?) index math.
def swap(ref_lst, snp_lst):
    count = 0
    for k in snp_lst:
        pos= int(k[0])
        list_ref=list(ref_lst)
        list_ref[pos] = k[2]      
        count += 1
    new=''.join(list_ref)
    print '%s SNPs processed' % (str(count))
    return new
    
#BODY

print '\n SNPpy V1.0\n============\n \/      \/\n || 0  0 ||\n \\\\_|__|_//\n (___<>___)\n ////  \\\\\\\\ \n ||||  ||||\n'

#Assign current working directory
cwd = os.getcwd()

#Dictionary relating 'chrn' (as found in 1001 files) string with path to corresponding TAIR10 Col-0 full chromosome sequence

#Assign chromosome of interest
chrom = 4
    
#Assign working refernce path

#Read in start and stop position as integers

#get minimum quality score
qual = int(raw_input('Enter a minimum quality score (inclusive) for SNPs to be processed. Scores less than 20 are not considered reliable. Scores above 25 are considered very reliable.\n> '))

#get file tag
tag = raw_input('Enter a tag to be appended to your translated files before the .fas extenstion.\n> ').lower()

#create short sequence with correct chromosomal index


f_name='/Users/greymonroe/Dropbox/CBF/SNPpy/ref_files/TAIR10_chr4.fas'
chr4 = " ".join(line.strip() for line in open(f_name,'r'))   
ref_seq=chr4.replace(' ','')  

#iterate over files in snp_files to generate new fasta files.
for i in os.listdir(cwd + '/snp_files'):
    path = cwd + '/snp_files/' + i
    if not path.endswith('~') and path.endswith('.txt'):
        print '\nconverting %s to fasta format' % (i)
        snps = get_snps(path, chrom, qual)
        with open(cwd + '/output/' + i[:-4] + '_' + tag + '.fas', 'w') as f:
            f.write('>' + i[:-4] + '_' + tag + '\n' + swap(ref_seq, snps))
    print 'done'

print '\n'


